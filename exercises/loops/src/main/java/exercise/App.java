package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String str) {
        String result = "";
        if (Character.isLetter(str.charAt(0))) {
            result = result + str.charAt(0);
        }
        for (int i = 1; i < str.length(); i++) {
            if (Character.isLetter(str.charAt(i)) && Character.isWhitespace(str.charAt(i - 1))) {
                result = result + str.charAt(i);
            }
        }
        return result.toUpperCase();
    }

    public static void main(String[] args) {
        System.out.println(App.getAbbreviation("Portable Network Graphics")); // "PNG"
        System.out.println(App.getAbbreviation("Complementary metal oxide semiconductor")); // "CMOS"
    }
    // END
}
