// BEGIN
package exercise.geometry;

import java.util.Arrays;

public class Segment {

    public static double[][] makeSegment(double[] pointX, double[] pointY) {
        double[][] segment = {
                {pointX[0], pointX[1]},
                {pointY[0], pointY[1]}
        };
        return segment;
    }

    public static double[] getBeginPoint(double[][] segment) {
        double[] BeginPoint = {segment[0][0], segment[0][1]};
        return BeginPoint;
    }

    public static double[] getEndPoint(double[][] segment) {
        double[] EndPoint = {segment[1][0], segment[1][1]};
        return EndPoint;
    }

    public static void main(String[] args) {
        double[] point1 = Point.makePoint(2, 3);
        double[] point2 = Point.makePoint(4, 5);
        double[][] segment = Segment.makeSegment(point1, point2);
        double[] beginPoint = Segment.getBeginPoint(segment);
        double[] endPoint = Segment.getEndPoint(segment);
        System.out.println(Arrays.toString(beginPoint)); // => [2, 3]
        System.out.println(Arrays.toString(endPoint)); // => [4, 5]
    }
}

// END
