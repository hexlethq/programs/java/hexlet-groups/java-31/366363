package exercise;


import java.util.Arrays;

class App {
public static int[] bubbleSort(int[]numbers) {
    int length = numbers.length;
    for (int i=0; i<length-1; i++) {
        for (int j=0; j<length-i-1;j++) {
            if (numbers[j] > numbers[j+1]) {
                int temp = numbers[j];
                numbers[j] = numbers[j+1];
                numbers[j+1] = temp;
            }
        }
    }
return numbers;
}
    public static void main(String[] args) {
        int[] numbers = {3, 10, 4, 3};
        App.bubbleSort(numbers);
        System.out.println(Arrays.toString(numbers)); // => [3, 3, 4, 10]
    }
}