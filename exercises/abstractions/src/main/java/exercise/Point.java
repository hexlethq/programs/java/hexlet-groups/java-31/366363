package exercise;

class Point {
    // BEGIN
    public static int[] makePoint(int XCoordinate, int YCoordinate) {
        int[] point = {XCoordinate, YCoordinate};
        return point;
    }

    public static int getX(int[] point) {
        int XCoord = point[0];
        return XCoord;
    }

    public static int getY(int[] point) {
        int YCoord = point[1];
        return YCoord;
    }

    public static String pointToString(int[] point) {
        String stringRepresentation = "(" + point[0] + ", " + point[1] + ")";
        return stringRepresentation;
    }

    public static int getQuadrant(int[] point) {
        int quadrantNumber = 0;
        if (point[0] > 0 && point[1] > 0) {
            quadrantNumber = 1;
        }
        if (point[0] < 0 && point[1] > 0) {
            quadrantNumber = 2;
        }
        if (point[0] < 0 && point[1] < 0) {
            quadrantNumber = 3;
        }
        if (point[0] > 0 && point[1] < 0) {
            quadrantNumber = 4;
        }
        if (point[0] == 0 || point[1] == 0) {
            quadrantNumber = 0;
        }
        return quadrantNumber;
    }

    public static void maim(String[] args) {
        int[] point = Point.makePoint(3, 4);
        System.out.println(Point.getX(point)); // 3
        System.out.println(Point.getY(point)); // 4
        System.out.println(Point.pointToString(point)); // "(3, 4)"

        System.out.println(Point.getQuadrant(point)); // 1
        int[] point2 = makePoint(3, -3);
        System.out.println(Point.getQuadrant(point2)); // 4
        int[] point3 = makePoint(0, 7);
        System.out.println(Point.getQuadrant(point3)); // 0
    }

    // END
}
