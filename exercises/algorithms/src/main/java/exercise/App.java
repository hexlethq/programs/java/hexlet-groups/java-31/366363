package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int[] sort(int[] numbers) {
        for (int j = numbers.length - 1; j >= 1; j--) {
            for (int i = 0; i < numbers.length - 1; i++) {
                if (numbers[i] > numbers[i + 1]) {
                    int tmp = numbers[i];
                    numbers[i] = numbers[i + 1];
                    numbers[i + 1] = tmp;
                }
            }
        }
        return numbers;
    }

    public static void main(String[] args) {
        int[] numbers = {3, 10, 4, 3};
        int[] sorted = App.sort(numbers);
        System.out.println(Arrays.toString(sorted)); // => [3, 3, 4, 10]
    }
    // END
}
