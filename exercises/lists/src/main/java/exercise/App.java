package exercise;

import java.util.ArrayList;

//BEGIN
public class App {
    public static boolean scrabble(String string, String word) {
        boolean result = false;
        String newString = string.toLowerCase();
        String newWord = word.toLowerCase();
        if (newString.length() >= newWord.length()) {
            ArrayList<Character> symbolsOfString = new ArrayList<>();
            for (int i = 0; i < newString.length(); i++) {
                symbolsOfString.add(newString.charAt(i));
            }
            ArrayList<Character> symbolsOfWord = new ArrayList<>();
            for (int j = 0; j < newWord.length(); j++) {
                symbolsOfWord.add(newWord.charAt(j));
            }
            for (int i = 0; i < symbolsOfWord.size(); i++) {
                if (symbolsOfString.remove(symbolsOfWord.get(i))) {
                    result = true;
                } else {
                    result = false;
                    break;
                }
            }
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(App.scrabble("rkqodlw", "world")); // true
        System.out.println(App.scrabble("ajv", "java")); // false
        System.out.println(App.scrabble("avjafff", "JaVa")); // true
        System.out.println(App.scrabble("", "hexlet")); // false
    }
}
//END
