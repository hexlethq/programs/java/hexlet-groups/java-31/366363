package exercise;
class App {

    public static boolean isBigOdd(int number) {
        // BEGIN
        boolean isBigOddVariable = number >= 1001 && number % 2 == 1;
        System.out.println(isBigOddVariable);
        // END
        return isBigOddVariable;
    }

    public static void sayEvenOrNot(int number) {
        // BEGIN
        if (number % 2 == 0) {
            System.out.println("yes");
        } else {
            System.out.println("no");
        }
        // END
    }


    public static void printPartOfHour(int minutes) {
        // BEGIN
        if (minutes >= 0 && minutes < 15) {
            System.out.println("First");
        }
        if (minutes >= 15 && minutes < 30) {
            System.out.println("Second");
        }
        if (minutes >= 30 && minutes < 45) {
            System.out.println("Third");
        }
        if (minutes >= 45 && minutes < 60) {
            System.out.println("Fourth");
        }

        // END
    }

    public static void main(String[] args) {
        App.isBigOdd(1001);
        App.isBigOdd(900);
        App.sayEvenOrNot(9);
        App.sayEvenOrNot(2);
        App.printPartOfHour(10);
        App.printPartOfHour(16);
        App.printPartOfHour(35);
        App.printPartOfHour(55);

    }
}



