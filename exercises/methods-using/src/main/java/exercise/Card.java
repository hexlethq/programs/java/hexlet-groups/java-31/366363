package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        String subString = cardNumber.substring(12, 16);
        String suString = cardNumber.substring(0, starsCount);

        System.out.println(suString.replaceAll("\\d", "*") + subString);
        // END
    }

    public static void main(String[] args) {
        Card.printHiddenCard("4567214587350971", 4);
        Card.printHiddenCard("4567214587350971", 3);
    }
}


