package exercise;

import java.util.Arrays;

class App {
    public static int[] reverse(int[] numbers) {
        int length = numbers.length;
        int[] reverseNumbers = new int[length];
        if (length == 0) {
            return numbers;
        }
        if (length > 0) {
            for (int i = 0; i < length; i++) {
                reverseNumbers[i] = numbers[(length - 1) - i];
            }
        }
        return reverseNumbers;
    }

    public static int mult(int[] numbers) {
        int length = numbers.length;
        int result = 1;
        for (int i = 0; i < length; i++) {
            result = result * numbers[i];
        }
        return result;
    }

    public static void main(String[] args) {
        int[] numbers = {1, 2, -6, 3, 8};
        int[] result = App.reverse(numbers);
        System.out.println(Arrays.toString(result)); // => [8, 3, -6, 2, 1]
        int[] numbers1 = {};
        System.out.println(App.mult(numbers1)); // 1
        int[] numbers2 = {1, 4, 3, 4, 5};
        System.out.println(App.mult(numbers2)); // 240
        int[] numbers3 = {1, 4, -3, 2};
        System.out.println(App.mult(numbers3)); // -24
        int[] numbers4 = {1, -3, 5, 4, -3, 0};
        System.out.println(App.mult(numbers4)); // 0
    }

}

