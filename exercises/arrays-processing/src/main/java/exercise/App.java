package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int getIndexOfMaxNegative(int[] numbers) {
        int length = numbers.length;
        int indexForMin = -1;

        if (length > 0) {
            int min = numbers[0];
            //      indexForMin = 0;
            for (int i = 1; i < length; i++) {
                int number = numbers[i];
                if (min > number && min > 0 && number < 0) {
                    min = number;
                    indexForMin = i;
                }
                if (min < 0 && number < 0 && Math.abs(min) > Math.abs(number)) {
                    min = number;
                    indexForMin = i;
                }
                if (min == numbers[0] && min < 0) {
                    indexForMin = 0;
                }
            }
        }
        return indexForMin;
    }


    public static int[] getElementsLessAverage(int[] numbers) {
        int length = numbers.length;
        int numberOfElementsIsLessThanTheAverage = 0;
        if (length > 0) {
            int sum = 0;
            for (int index = 0; index < length; index++) {
                sum = sum + numbers[index];
            }
            int average = sum / length;
            for (int index = 0; index < length; index++) {
                if (numbers[index] <= average) {
                    numberOfElementsIsLessThanTheAverage++;
                }
            }
            int newArrIndex = 0;
            int[] result = new int[numberOfElementsIsLessThanTheAverage];
            for (int index = 0; index < length; index++) {
                if (numbers[index] <= average) {
                    result[newArrIndex] = numbers[index];
                    newArrIndex++;
                }
            }
            return result;
        }
        return numbers;
    }

    public static void main(String[] args) {
        int[] numbers3 = {1, 4, -3, 4, -5};
        System.out.println(getIndexOfMaxNegative(numbers3)); // 2
        int[] numbers1 = {};
        System.out.println(getIndexOfMaxNegative(numbers1)); // -1
        int[] numbers2 = {1, 4, 3, 4, 5};
        System.out.println(getIndexOfMaxNegative(numbers2)); // -1
        int[] numbers4 = {1, -3, 5, 4, -3, -10};
        System.out.println(getIndexOfMaxNegative(numbers4)); // 1
        int[] numbers = {1, 2, 6, 3, 8, 12};
        int[] result = App.getElementsLessAverage(numbers);
        System.out.println(Arrays.toString(result)); // => [1, 2, 3]

    }
}

