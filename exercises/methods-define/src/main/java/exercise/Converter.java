package exercise;
class Converter {
    // BEGIN
    public static int convert(int chislo, String izmerenie) {
        int conv = 1;
        if ("Kb".equals(izmerenie)) {
            return conv = chislo / 1024;
        }
        if ("b".equals(izmerenie)) {
            return conv = chislo * 1024;
        } else {
            return conv = 0;
        }
    }

    public static void main(String[] args) {
        System.out.println("10 Kb = " + Converter.convert(10, "b") + " b");
    }
    // END
}
