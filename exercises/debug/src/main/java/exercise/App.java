package exercise;
class App {
    // BEGIN
    public static String getTypeOfTriangle(int a, int b, int c) {
        if ((a + b) > c && (b + c) > a && (a + c) > b) {
            if (a == b && a != c || b == c && a != b || a == c && b != c) {
                return "Равнобедренный";
            }
            if (a == b && a == c) {
                return "Равносторонний";
            }
            if (a != b || b != c || a != c) {
                return "Разносторонний";
            }
        } else {
            return "Треугольник не существует";
        }

        return null;
    }

    public static void main(String[] args) {

        System.out.println(App.getTypeOfTriangle(1, 2, 7)); // "Треугольник не существует"
        System.out.println(App.getTypeOfTriangle(5, 6, 7)); // "Разносторонний"
        System.out.println(App.getTypeOfTriangle(1, -2, 7)); // "Треугольник не существует"
    }


    // END
}
